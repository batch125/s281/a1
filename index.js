let http = require("http");

let port = 3000;

http.createServer((req,res)=>
	{
		if (req.url == "/login"){
			res.writeHead(200,{"content-Type":"text/plain"});
			res.end("Welcome to login page.")
		}
		else {
			res.writeHead(404,{"content-Type":"text/plain"})
			res.end("I'm sorry the page you are looking for cannot be found")
		}

	}).listen(port);

console.log("Server is successfully running")